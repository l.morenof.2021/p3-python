#! /usr/bin/env python3
from mysound import Sound


class SoundSin(Sound):

    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        self.sin(frequency, amplitude)


if __name__ == "__main__":
    # Crear un objeto SoundSin con una duración de 1 segundo,
    # frecuencia de 440 Hz y amplitud de 0.5
    sound_sin = SoundSin(duration=1, frequency=440, amplitude=100000)

    print("Señal senoidal:")
    print(sound_sin.bars())
