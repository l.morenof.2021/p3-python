#! /usr/bin/env python3

from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    duration = max(s1.duration, s2.duration)
    result_soundd = Sound(duration)

    for i in range(result_soundd.nsamples):
        if i < s1.nsamples and i < s2.nsamples:
            result_soundd.buffer[i] = s1.buffer[i] + s2.buffer[i]
        elif s1.nsamples > i >= s2.nsamples:
            result_soundd.buffer[i] = s1.buffer[i]
        elif s2.nsamples > i >= s1.nsamples:
            result_soundd.buffer[i] = s2.buffer[i]
    return result_soundd


if __name__ == "__main__":
    s1 = Sound(duration=3)
    s1.sin(frequency=220, amplitude=100000)
    s2 = Sound(duration=3)
    s2.sin(frequency=660, amplitude=1000000)
    result_sound = soundadd(s1, s2)
    print("Sound 1:")
    print(s1.bars())
    print("Sound 2:")
    print(s2.bars())
    print("Result Sound:")
    print(result_sound.bars())
