#! /usr/bin/env python3
import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_comprobacion(self):
        sound_sin = SoundSin(duration=1, frequency=440, amplitude=0.5)
        self.assertIsInstance(sound_sin, SoundSin)
        self.assertEqual(sound_sin.duration, 1)
        self.assertEqual(sound_sin.samples_second, 44100)
        # comprueba que la onda seno tenga la duracion y numero de muestras correcto

    def test_amplitude(self):
        sound_sin = SoundSin(duration=1, frequency=440, amplitude=0.5)
        self.assertEqual(sound_sin.max_amplitude, 2 ** 15 - 1)
        # comprueba que se cumple que la amplitud maxima de la onda seno sea 2**15 - 1

    def test_buffer_len(self):
        sound_sin = SoundSin(duration=1, frequency=440, amplitude=0.5)
        self.assertEqual(len(sound_sin.buffer), sound_sin.nsamples)
        # comprueba que el tamaño del buffer sea igual al numeros de muestras


if __name__ == '__main__':
    unittest.main()
