#! /usr/bin/env python3
import unittest
from mysound import Sound


class Test(unittest.TestCase):
    def test_init(self):
        sound = Sound(1)
        self.assertEqual(sound.duration, 1)
        self.assertEqual(sound.nsamples, 44100)


if __name__ == '__main__':
    unittest.main()

# este test comprueba que la duracion de mysound sea 1 segundo y
# el numero de muestras sea el esperado para 1 segundo a 44100 muestras/segundo
# si en mysound en modify run configuration, en el primer parametro (duration) ponemos 1, deberia pasar este test
# De lo contrario , daria error
# Ademas, en la clase sound de mysound, tenemos como parametro samples_second = 44100
# Este programa comprueba que no se cambie ese parametro, ya que daria error en el test
